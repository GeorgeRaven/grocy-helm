# Grocy Helm

This has been moved to https://gitlab.com/GeorgeRaven/raven-helm-charts

A helm chart for grocy, the household ERP.

This helm chart provides easy deployment of grocy on kubernetes clusters.
This chart uses https://hub.docker.com/r/linuxserver/grocy as the base image.
This also creates a barcode buddy instance to streamline barcode scanning.

## Quick Start

```bash
helm repo add grocy https://gitlab.com/api/v4/projects/53525725/packages/helm/stable
helm repo update grocy
helm install grocy grocy/grocy
```
